import pygame
import cv2
import os

PROGRAM_PATH = os.path.dirname(os.path.realpath(__file__))
videos = {}
fotos = {}
RATIO = 0.5 * 0.9


# reading image
img = cv2.imread(f"{PROGRAM_PATH}/bottle.png")

# converting image into grayscale image
imggray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

# setting threshold of gray image
_, threshold = cv2.threshold(imggray, 127, 255, cv2.THRESH_BINARY)

# using a findContours() function
contours, _ = cv2.findContours(threshold, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

def is_inside(xpos,ypos):
  res = cv2.pointPolygonTest(contours[1],(xpos / RATIO, ypos / RATIO),True)
  #return res >= -35
  return res >= -55

x = int(1351 * RATIO)
y = int(2160 * RATIO)
window = pygame.display.set_mode((x, y))
background = pygame.image.load(f"{PROGRAM_PATH}/bottle.png")
#.convert_alpha()
background = pygame.transform.scale(background, (x, y))
run = True
WIDTH_FRAME = int(58 * RATIO)
INIT_BEER_X = int(390 * RATIO)
LAST_BEER_X = int(990 * RATIO)
x_frame = INIT_BEER_X
INIT_Y_FRAME = int(2105 * RATIO)
y_frame = INIT_Y_FRAME
actual_y_frame = int(2160 * RATIO)
TOP_Frames = {}
LAST_BEER_Y = 250


square = pygame.image.load(f"{PROGRAM_PATH}/square.png")

width_beer = LAST_BEER_X - INIT_BEER_X
TOP_Frames = {}
for item in range(int(width_beer/WIDTH_FRAME)):
  TOP_Frames[INIT_BEER_X + (item * WIDTH_FRAME)] = y_frame

def get_list_path(path):
  if os.path.isdir(f"{PROGRAM_PATH}//{path}"):
    program_path = os.path.dirname(os.path.realpath(__file__))
    files = os.listdir(f"{PROGRAM_PATH}//{path}")
    if '.DS_Store' in files: files.remove('.DS_Store')
    if '.tmp.drivedownload' in files: files.remove('.tmp.drivedownload')
    full_path = [ f"{PROGRAM_PATH}//{path}//{filepath}" for filepath in files ]
    return sorted(full_path, key=lambda x: int(x.split('.')[0].split('_')[-1]))
  else:
    return []

def get_middle_frame(filename):
  cap = cv2.VideoCapture(filename)
  totalFrames = cap.get(cv2.CAP_PROP_FRAME_COUNT)
  cap.set(cv2.CAP_PROP_POS_FRAMES, int(totalFrames/2))
  return cap.read()

count = 0

def update_position(height):
    global actual_y_frame
    global x_frame
    global y_frame
    global count
    x_frame += WIDTH_FRAME
    TOP_Frames[INIT_BEER_X + (count * WIDTH_FRAME)] = TOP_Frames[INIT_BEER_X + (count * WIDTH_FRAME)] - height
    count += 1
    if count >= int(width_beer/WIDTH_FRAME):
        x_frame = INIT_BEER_X
        actual_y_frame -= height
        count = 0

INDEX_VIDEO = 0
PLAY_VIDEO = None
def playVideo():
    global PLAY_VIDEO
    global INDEX_VIDEO
    if not PLAY_VIDEO:
        videos = get_list_path('videos')
        if len(videos) == 0: return
        PLAY_VIDEO = cv2.VideoCapture(videos[INDEX_VIDEO])
    success, video_image = PLAY_VIDEO.read()
    if success:
        (w, h) = video_image.shape[1::-1]
        video_image = cv2.resize(video_image, (int(w/5), int(h/5)), interpolation=cv2.INTER_AREA)
        video_surf = pygame.image.frombuffer(video_image.tobytes(), video_image.shape[1::-1], "BGR")
        window.blit(video_surf, (0, 0))
    else:
        INDEX_VIDEO += 1
        PLAY_VIDEO = None
        videos = get_list_path('videos')
        if len(videos) <= INDEX_VIDEO:
            INDEX_VIDEO = 0
         

def play():
    global count
    if actual_y_frame > LAST_BEER_Y:
        for foto_path in get_list_path('fotos'):

            if foto_path not in fotos:
                foto = pygame.image.load(foto_path)
                (width,height) = foto.get_rect().size
                height = int(WIDTH_FRAME * height / width)
                retries = 0
                while not is_inside(x_frame, TOP_Frames[INIT_BEER_X + (count * WIDTH_FRAME)] - height) and retries < 10:
                    retries +=1
                    update_position(height)
                if actual_y_frame > LAST_BEER_Y:
                    xpos = x_frame
                    ypos = TOP_Frames[INIT_BEER_X + (count * WIDTH_FRAME)] - height
                    update_position(height)

                    foto = pygame.transform.scale(foto, (WIDTH_FRAME, height))
                    fotos[foto_path] = { 'foto': foto, 'xpos': xpos, 'ypos': ypos }
                    window.blit(foto, (xpos, ypos))


            else:
                foto = fotos[foto_path]['foto']
                xpos = fotos[foto_path]['xpos']
                ypos = fotos[foto_path]['ypos']
                window.blit(foto, (xpos, ypos))
        for video_path in get_list_path('videos'):
            if video_path not in videos:
                success, video_image = get_middle_frame(video_path)
                if success:
                    (width,height) = video_image.shape[1::-1]
                    height = int(WIDTH_FRAME * height / width)
                    retries = 0
                    while not is_inside(x_frame, TOP_Frames[INIT_BEER_X + (count * WIDTH_FRAME)] - height) and retries < 10:
                        retries +=1
                        update_position(height)
                    if actual_y_frame > LAST_BEER_Y:
                        xpos = x_frame
                        ypos = TOP_Frames[INIT_BEER_X + (count * WIDTH_FRAME)] - height
                        update_position(height)

                        video_image = cv2.resize(video_image, (WIDTH_FRAME, height), interpolation=cv2.INTER_AREA)
                        video_surf = pygame.image.frombuffer(video_image.tobytes(), video_image.shape[1::-1], "BGR")
                        videos[video_path] = { 'video_surf': video_surf, 'xpos': xpos, 'ypos': ypos }
                        window.blit(video_surf, (xpos, ypos))

            else:
              video_surf = videos[video_path]['video_surf']
              xpos = videos[video_path]['xpos']
              ypos = videos[video_path]['ypos']
              window.blit(video_surf, (xpos, ypos))

clock = pygame.time.Clock()

if __name__ == "__main__":
    while run:
        clock.tick(30)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
        play()
        window.blit(background, (0, 0))
        '''
        for contour in contours[1]:
            point = contour[0]
            x = point[0]
            y = point[1]
            window.blit(square, (x * RATIO,y * RATIO))
        '''
        playVideo()
        pygame.display.flip()

    pygame.quit()
    exit()
